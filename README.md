# specs

Documentation and specification

# Main attributes of charity organizations

1. trustworthiness: is the organisation credible? how long are they in the field?
2. effectiveness: how well is it organized? which percentage of donations does reach those in need? 
3. performance: how fast do they arrive at their destination?
4. category: what topics are the donations for?
5. topicality: how up-to-date are the topics?
6. locality: where is the target area located?


